# Arcades

Algumas máquinas tem orientação vertical.

##### Bugs conhecidos

O hardware que gera as estrelas de fundo do Galaxian não esta corretamente sintetizado.

Super Glob parece que usa um botão de ação, que não está sintetizado no hardware.

Super Cobra está com cores trocados e som ainda não completamente sintetizado.

Scramble, Frogger e Super Cobra usa uma temporização não padrão, que pode não ser compatível com todos os monitores 

##### Change log

- 008 - 25/03/2020 - Adicionado Arcades da Midway/Taito 8080. Lunar Rescue, Ozma Wars, Vortex, Baloon Bomber, Space Laser, Super Earth Invasion, Space Invaders e Space Invaders Deluxe

- 007 - 24/10/2019 - Adicionado Centipede.

- 006 - 23/10/2019 - Adicionado Asteroids.

- 005 - 16/10/2019 - Adicionado Super Cobra (preliminar). Scramble e Frogger substituido por versões novas, com suporte a VGA.

- 004 - 11/10/2019 - Adicionado New Rally X

- 003 - 30/07/2019 - Jogo River Raid em FPGA. Convertido para o MC por Mauricio Melo.

- 002 - 24/12/2016 - hardware do Pac-man atualizado e ampliado. Incluido Roller Crusher, Gorkans, Mr TNT, Ms Pac-Man e Super Glob. Versão inicial do hardware da Galaxian.

- 001 - 24/11/2016 - versão inicial Pac-Man, Scramble e Frogger