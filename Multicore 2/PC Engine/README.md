# PC Engine

Conversão do PC Engine/Turbo Grafx para o Multicore 2.

##### Montando o cartão SD
#
###### 1a. Opção - Com o cartão somente com este core
Formate um cartão SD em FAT ou FAT 32.
Copie e renomeie o arquivo "FPGAPCE.MC2" para "CORE.MC2" na raiz do cartão SD. 
Coloque as ROMs, geralmente com a extensão ".PCE", no cartão.
###### 2a. Opção - Com o cartão compartilhado com outros cores
Se o seu cartão já possui outros cores, copie o arquivo "FPGAPCE.MC2" para a raiz do cartão SD, junto com os outros .MC2 já existentes.
Por motivos de organização, crie uma pasta especifica para as ROMs deste core e copie.

##### Utilizando o Core
Botão 1 - Reset
A qualquer momento a tecla F12 pode ser usada para carregar uma ROM.
A maioria das ROMs funciona com o modo "PC Engine", selecionado no menu,
mas para alguns é necessário a troca para o modo "Turbo Grafx".

Verifique a lista de compatibilidade e o modo de jogo (PC Engine ou Turbo Grafx) em
https://docs.google.com/spreadsheets/d/1SsyP4agbmYCnU2xwXruKzebQctOcPypf9BdYlCPxmWs/edit#gid=0

##### Joystick
Este core necessita de um joystick com multiplos botões, então é recomendado o uso de um Sega de 3 ou 6 botões.

O teclado pode ser utilizado:

JOGADOR 1
Direcionais =  Setas cursoras
Botão I = Ctrl direito
Botão II = AltGr (Alt direito)
Run = Enter

JOGADOR 2
Direcionais =  ASDW
Botão I = Ctrl esquerdo
Botão II = Alt esquerdo
Run = Caps Lock

##### Change log

- 003 : 20/08/2018 - Emulação de joystick pelo teclado
- 002 : 28/03/2018 - Melhorias no som e adição do "pixelado" na saída de vídeo.
- 001 : 26/01/2018 - versão inicial.