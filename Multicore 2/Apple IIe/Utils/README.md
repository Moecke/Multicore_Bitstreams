# Imagens de disco

A controladora do Apple II espera uma sequencia de discos "nibblized" (227k) no SD a partir do bloco 0. 
Muitas imagens de disco de Apple est�o em formato DSK de 140Kb, ent�o precisam ser convertidas antes para o formato NIB. O pequeno programa "AppleDSK2NIB.exe" cumpre este trabalho, convertendo a imagem.
