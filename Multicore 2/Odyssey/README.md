# Odyssey

Formato do SD: FAT16 ou FAT32, com suporte a nomes longos de arquivo.

Tecla F12 por 1 segundo abre a tela do SD Loader

Scanlines opcionais: Botão 1 + Botão 2 ativam/desativam scanlines

Descompactar o arquivo zip na raiz do cartão SD.

##### Bugs conhecidos

- Super Bee (Brasil): Na tela de game over, as patas da aranha são desenhadas incorretamente. A versão Super Bee (Europe) funciona corretamente.


##### Change log

- 005 : 04/08/2019 - Adicionado módulo "The Voice"
- 004 : 03/09/2018 - Acerto no clock, intro do Killer Bees aparece corretamente agora. 
- 003 : 12/03/2018 - versão Multicore 2
- 002 : 25/11/2016 - Scanlines opcionais incluidas
- 001 : 24/11/2016 - versão inicial