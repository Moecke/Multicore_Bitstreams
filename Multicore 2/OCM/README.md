# MSX 2+

Conversão do One Chip MSX para o Multicore 2.

Descompacte o arquivo OCM.RAR para obter o arquivo OCM.img. Use o Etcher ou Win32DiskImager para gravar essa imagem em um cartão SD (mínimo 1 Gb).

Após a gravação, copiar o arquivo "OCM.MC2" para a raiz do cartão. Renomeie-o para "core.MC2" se você deseja que o boot seja automático.

Baseado no código de Luca "KdL" Chiodi. Arquivos originais em
https://gnogni.altervista.org/

# Outros

Botão 1 - Reset

##### Change log

- 008 : 24/09/2019 - Suporte a OPL-OPL3
- 007 : 24/06/2019 - KdL versao 3.7.1
- 006 : 26/02/2019 - Correção no DAC de cores, Correção no pull-up do barramento
- 005 : 14/08/2018 - Correção nos switchs (invertidos)
- 004 : 29/07/2018 - KdL versao 3.6.2
- 003 : 07/07/2018 - KdL versao 3.6.1
- 002 : 15/05/2018 - KdL versao 3.6
- 001 : 26/01/2018 - versão inicial